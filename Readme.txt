Instructions to run the code.
1. Execute the script file main_bb.m
2. The trajectroy data gets stored in a folder named traj.
3. Path of the trajectroy data file is printed on the console.

Instructions to view the results
1. In the script file main_display.m in line number 53 update the path of the trajectory data. 
1. Execute the script main_display.m
2. Video gets saved in the the same location as the trajectroy data under a folder named as Results.
